//Christian D. Walker
//Lab 3 - Rect Class
public class Rect extends Quad
{

    public Rect(double length, double width)
    {

      setHeight(height);
      setWidth(width);
    }
    public void calculateArea()
    {
        setArea(getHeight()*getWidth());
    }

    public void calculatePerimeter()
    {
        setPerimeter((getHeight()*2)+(getWidth()*2));
    }


}
