//Christian D. Walker
//Lab 3 - Circle Class

public class Circle extends Round
{
  protected final double radius;
    public Circle(double radius)
    {
       this.radius = radius;

    }


    public void calculateArea()
    {
        setArea(Math.PI*(radius*radius));
    }

    public void calculatePerimeter()
    {
        setPerimeter(2*(Math.PI*radius));
    }
}
