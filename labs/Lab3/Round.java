//Christian D. Walker
//Lab 3 - Round Class
public class Round extends Shape
{
    private double pi = 3.1415926535;

    public double getPi()
    {
        return pi;
    }
}
