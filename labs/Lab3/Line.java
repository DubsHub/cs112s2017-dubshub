//Christian D. Walker
//Lab 3 - Line Class
public class Line extends Shape
{
    private final double x1, x2, y1, y2;
     public Line(double x1, double y1, double x2, double y2)
    {
    this.x1 = x1;
    this.x2 = x2;
    this.y1 = y1;
    this.y2 = y2;

    }

    public void calculateArea()
    {
     setArea(0);
    }

    public void calculatePerimeter()
    {
     setPerimeter(2*Math.sqrt(Math.pow(x2-x1, 2)+ Math.pow(y2-y1, 2)));

   }
}
