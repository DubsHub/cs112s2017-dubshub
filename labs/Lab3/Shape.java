//Christian D. Walker
//Lab 3 - Shape Class

public class Shape
{

      protected double perimeter;
      protected double area;


      public double getArea()
      {
          return area;
      }

      public double getPerimeter()
      {
          return perimeter;
      }

      public void setPerimeter(double perimeter)
      {
          this.perimeter = perimeter;
      }

      public void setArea(double area)
      {
          this.area = area;
      }

}
