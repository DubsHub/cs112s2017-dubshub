//Christian D. Walker
//Lab 3 - Trapzd Class
public class Trapzd extends Quad
{
    private final double otherBase, side1, side2;
    public Trapzd(double base1, double height, double base2, double side1, double side2)
    {
        this.otherBase = base2;
        this.side1 = side1;
        this.side2 = side2;
        setHeight(height);
        setWidth(base1);

    }

    public void calculateArea()
    {
        setArea(getHeight()*((getWidth()+otherBase)/2));
    }

    public void calculatePerimeter()
    {
        setPerimeter(getWidth()+ otherBase+ side1+ side2);
    }
}
