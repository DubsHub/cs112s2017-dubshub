//Christian D. Walker
//Lab 3 - Square Class
public class Square extends Rect
{

    public Square(double length)
    {
        super(length, length);
    }

    public void calculateArea()
    {
        setArea(getWidth()*getWidth());
    }

    public void calculatePerimeter()
    {
        setPerimeter(getWidth()*4);
    }
}
