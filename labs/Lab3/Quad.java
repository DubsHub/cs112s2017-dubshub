//Christian D. Walker
//Lab 3 - Quad Class

public class Quad extends Shape
{
    protected double width;
    protected double height;

    public double getWidth()
    {
        return width;
    }

    public double getHeight()
    {
        return height;
    }

    public void setWidth(double w)
    {
       width = w;
    }

    public void setHeight(double h)
    {
        height = h;
    }
}
