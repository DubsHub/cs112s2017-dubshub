//Christian D. Walker
//Lab 3 - Oval Class
public class Oval extends Round
{
   protected final double major, minor;
    public Oval(double major, double minor)
    {

    this.major = major;
    this.minor = minor;

    }
    public void calculateArea()
    {
       setArea(Math.PI*major*minor);
    }

    public void calculatePerimeter()
    {
       double temp = Math.pow(major,2);
       double temp2 = Math.pow(minor, 2);
       double temp3 = temp + temp2;
       double temp4 = temp3/2;
       double temp5 = Math.sqrt(temp4);
       double temp6 = 2*Math.PI*temp5;
       setPerimeter(temp6);




}
}
