//*********************************
// Honor Code: The work we are submitting is the result of our own thinking and efforts.
// Christian Walker
// Lab 02 Part 2
// Purpose: Guess my number, human player
//********************************* 
import java.util.Scanner;
import java.util.Random;
public class Lab2Pt2
{
	public static void main (String [] args)
	{


		//my variables
		Scanner scan = new Scanner(System.in);
		Random randomNumber = new Random();
		int numberOfTries = 0;
		int correct = 0;
		int max = 100;
		int min =  1;
		int compGuess = (min+max)/2; 
			int input;
			//guess

		while(true){	// going to use while else/if
			System.out.println("Think of a number between 1-100 in your head!");
			do {
				System.out.println("Is this your number?:"+compGuess);
				
				System.out.println("Type 1 if it's too high, -1 if it's too low or 0 if it's correct");
				input = scan.nextInt();

				if ( input == 1 ){ max = compGuess; 

				} else if ( input == -1){ min = compGuess;
                
              			} else {
					break;
                
                
				}
				 compGuess = (min+max)/2;
              		} while (true);
	
 			System.out.println("Do you want to play again? (1:Yes/2:No)");
    // if they respond yes leave 2nd loop if no finish
    			if(scan.nextInt() != 1) break; //finish
    		}
	}

}
