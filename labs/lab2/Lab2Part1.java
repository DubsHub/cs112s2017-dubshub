//*********************************
// Honor Code: The work we are submitting is the result of our own thinking and efforts.
// Christian Walker
// Lab 02 Part 1
// Purpose: Guess my number, human player
//********************************* 
import java.util.Scanner;
import java.util.Random;
public class Lab2Part1
{
   public static void main (String [] args)
   {


  //my variables
   Scanner scan = new Scanner(System.in);
   Random randomNumber = new Random();
  int computerValue = randomNumber.nextInt(100);
  int numberOfTries = 0;
  int success = 0;
  int gg = 0; //guess
 // going to use while else/if

while(true) {
    computerValue = randomNumber.nextInt(100);
    numberOfTries = 0;
    while (true) {
        System.out.println("Enter a number between 1-100: ");
        gg = scan.nextInt();
        //scan line for the number to be guessed
        numberOfTries++;
          //
        if (gg < 1 || gg > 100) System.out.println("Invalid input");
        else if (gg == computerValue) {
            System.out.println("Congratulations you won! Your numbers of tries was: " + numberOfTries + " and the number was: " + computerValue);
            // leave the first loop
            break; //finish
        }
        else if (gg < computerValue) System.out.println("Your guess is too low!");
        else if (gg > computerValue) System.out.println("Your guess is too high!");
    } //end else/if

    System.out.println("Do you want to play again? (1:Yes/2:No)");
    // if they respond yes leave 2nd loop if no finish
    if(scan.nextInt() != 1) break; //finish
    
}
}
}
