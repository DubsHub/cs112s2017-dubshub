import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.List;
import java.util.Collections;
import java.util.LinkedList;

public class Wargame {
	public static String playerName = "";

	public static float INTERVAL_SEC = 0.3f;

	public static void addInterval() {//adds a time interval
		try {
			Thread.sleep((long) (INTERVAL_SEC * 1000));//program sleeps for given milliseconds
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public static void main(String[] args) {
		int round = 1;//initialize the round as 1
		List<Item> deckOfCards = new ArrayList<Item>();//this is a deck of cards

		for (int x = 0; x < 4; x++) {
			for (int y = 2; y < 15; y++) {
				deckOfCards.add(new Item(x, y));//populating the deck with the cards with respective type like 2 of Spades, 3 of Spades ... Ace of Diamonds 
			}
		}

		Collections.shuffle(deckOfCards, new Random());//randomize the deck, ie Shuffling the deck of card

		LinkedList<Item> playersCards = new LinkedList<Item>();//list of players card
		LinkedList<Item> computersCards = new LinkedList<Item>();//list of computers card

		playersCards.addAll(deckOfCards.subList(0, 26));//add cards from index 0 t0 25 in players deck
		computersCards.addAll(deckOfCards.subList(26, deckOfCards.size()));//add cards from index 26 t0 52 in computers deck

		System.out.println("Enter your name");
		playerName = new Scanner(System.in).nextLine();//gets the name of player and stores in the string playerName

		while (true) {//continues the loop unless soomething breaks. In our case the loops continues unless the game is won by either side
			if (round < 10) {//0 to 9 costs one space so maitaining the space of Round # in the display
				System.out.println("\n----------------\n----Round " + round + "-----\n----------------");
			} else {
				System.out.println("\n----------------\n----Round " + round + "----\n----------------");
			}
			addInterval();//add an interval
			System.out.println(playerName + ": " + playersCards.size());
			addInterval();//add an interval
			System.out.println("Computer: " + computersCards.size());
			addInterval();//add an interval
			System.out.println("Enter 'DRAW' to draw cards from Deck");
			String ss = new Scanner(System.in).nextLine();
			if (ss.toLowerCase().equalsIgnoreCase("draw")) {//continues when player enters 'draw'
				
				Item p1Card = playersCards.pop();// gets the card from the top of the players deck
				Item p2Card = computersCards.pop();//gets the card from the top of the computers deck
				
				addInterval();//add an interval
				System.out.println(playerName + ": " + p1Card.toString());
				
				addInterval();//add an interval
				System.out.println("Computer: " + p2Card.toString());

				addInterval();//add an interval
				if (p1Card.getCard() > p2Card.getCard()) {//if players card is greater than computer then player win the round
					playersCards.addLast(p1Card);// adds the players card in its own deck
					playersCards.addLast(p2Card);//also adds the computers card in players deck because player won this round
					System.out.println("Wohoo you have won the round!");
				}

				else if (p1Card.getCard() < p2Card.getCard()) {//if computers card is greater than players then computer win the round
					computersCards.addLast(p1Card);// adds the players card in computers deck because computer won this round
					computersCards.addLast(p2Card);//also adds the computers card in its own deck 
					System.out.println("Damn! Computer won it this time");
				}

				else {//both cards are equal
					addInterval();//add an interval
					System.out.println("Computer : War");
					
					addInterval();//add an interval
					System.out.print(playerName + ": ");

					String warString = new Scanner(System.in).next();//gets the text war from player
					if (warString.toLowerCase().equalsIgnoreCase("war")) {//continues when player enters war

						List<Item> warCardsforPlayer = new ArrayList<Item>();//creates a list for the cards for player in war round
						List<Item> warCardsforComputer = new ArrayList<Item>();//creates a list for the cards for computer in war round

						for (int x = 0; x < 3; x++) {//This loop adds three cards from the top of each player to their respective war card deck

							if (playersCards.size() == 0 || computersCards.size() == 0) {//if either of their card gets to zero then the game is over
								break;
							}

							System.out.println(""+playerName+" : xx\nComputer : xx");

							warCardsforPlayer.add(playersCards.pop());//take the card from the top of player to the war deck of player
							warCardsforComputer.add(computersCards.pop());//take the card from the top of computer to the war deck of computer
						}
						warCardsforPlayer.add(playersCards.pop());//draw the fourth card from the players deck to the players war card deck
						warCardsforComputer.add(computersCards.pop());//draw the fourth card from the computers deck to the computers war card deck

						if (warCardsforPlayer.size() == 4 && warCardsforComputer.size() == 4) {//check if the war card deck of each player is 4. Continues if both of their deck has four cards

							System.out.println(playerName+" : " + warCardsforPlayer.get(3).toString());//shows the fourth card of players war deck
							System.out.println("Computer : " + warCardsforComputer.get(3).toString());//shows the fourth card of computers war deck

							if (warCardsforPlayer.get(3).getCard() > warCardsforComputer.get(3).getCard()) {//if players fourth card is greater then player wins the war round
								playersCards.addAll(warCardsforPlayer);//adds all card from players war deck to players deck of cards
								playersCards.addAll(warCardsforComputer);//adds all card from computers war deck to players deck of cards because player won the war round
								System.out.println("You won WAR");
							}

							else {//if computers fourth card is greater then computer wins the war round
								computersCards.addAll(warCardsforPlayer);//adds all card from players war deck to computers deck of cards because computer won the war round
								computersCards.addAll(warCardsforComputer);//adds all card from computers war deck to its own deck of cards
								System.out.println("COMPUTER won WAR");
							}
						}
					}

				}
					if (playersCards.size() == 0) {//check if player has zero cards
						System.out.println("=================\n===YOU LOST :(===\n=================");
						break;
					} else if (computersCards.size() == 0) {//check if computer has zero cards
						System.out.println("================\n===YOU WON :)===\n================");
						break;
					}

					System.out.println("\n=== Hit enter to start next round! ===");
					if (new Scanner(System.in).hasNextLine()){}
			}
			round++;
		}

	}
}
