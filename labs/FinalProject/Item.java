
public class Item {
	private int cardNumber;
    private int cardType; 
    
    public Item(int cardType, int cardNumber){
        this.cardNumber = cardNumber;
        this.cardType = cardType;
    }
    
    public int getCard(){
        return cardNumber; 
    }
    
    public void setCard(int rank){
        this.cardNumber = rank;
    }
    
    @Override
    public String toString(){
        StringBuilder displayCard = new StringBuilder();
        switch(cardNumber){
            case 11:
                displayCard.append("Jack");
                break;
            case 12:
                displayCard.append("Queen");
                break;
            case 13:
                displayCard.append("King");
                break;
            case 14:
                displayCard.append("Ace");
                break;    
            default:
                displayCard.append(cardNumber);
                break;
        }
        
        displayCard.append(" of ");
        
        switch(cardType){
            case 0:
                displayCard.append("Spades");
                break;
            case 1:
                displayCard.append("Hearts");
                break;
            case 2:
                displayCard.append("Clubs");
                break;
            case 3:
                displayCard.append("Diamonds");
                break;
            default:
                break;
        }
        
        return displayCard.toString();
    }
}
