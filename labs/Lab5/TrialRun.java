import java.util.Scanner;
import java.util.LinkedList;
import java.util.Iterator;
public class TrialRun
{

	public static void main(String args[])
	{

		int N = 100000;
		long startTime = System.nanoTime();
		int[] myArray = new int[N];
		for (int i = 0; i < N; i++) {
			myArray[i] = i;
		} //for
		// Add to Java’s LinkedList
		long endTime = System.nanoTime();
		long runTime = endTime - startTime;
		System.out.println("Adding to an array " +runTime);

		startTime = System.nanoTime();
		LinkedList<Integer> myList = new LinkedList<Integer>();
		for (int i = 0; i < N; i++) {
			myList.add(i);
		} //for
		// Add to our LinkedList
		endTime = System.nanoTime();
		runTime = endTime - startTime;
		System.out.println("Adding to an LinkedList " +runTime);

		startTime = System.nanoTime();

		LL mySList = new LL();
		for (int i = 0; i < N; i++) {
			mySList.add(i);
		} //for
		// Add to our Linked List
		endTime = System.nanoTime();
		runTime = endTime - startTime;
		System.out.println("Adding to a LL " +runTime);

		startTime = System.nanoTime();

		IteratingGenericLL<Integer> myIList = new IteratingGenericLL<Integer>();
		for (int i = 0; i < N; i++) {
			myIList.add(i);

		} //for
		// Add to our IteratingGenericLL
		endTime = System.nanoTime();
		runTime = endTime - startTime;
		System.out.println("Adding to IteratingGenericLL " +runTime);

		startTime = System.nanoTime();


		// Get from array
		for (int i = 0; i < N; i++) {
			int something = myArray[i];
		} //for

		endTime = System.nanoTime();
		runTime = endTime - startTime;
		System.out.println("Get from array " +runTime);

		startTime = System.nanoTime();
		// Get from Java’s LinkedList

		for (int i = 0; i < N; i++) {
			int variable = myList.get(i);
		} //for

		endTime = System.nanoTime();
		runTime = endTime - startTime;
		System.out.println("Get from LinkedList " +runTime);

		startTime = System.nanoTime();
		// Get from our LinkedList
		for (int i = 0; i < N; i++) {
			int somethingElse = myIList.get(i);
		} 
		//for
		endTime = System.nanoTime();
		runTime = endTime - startTime;
		System.out.println("Get from LL " +runTime);
		startTime = System.nanoTime();



		// Get from our IteratingGenericLL
		Iterator<Integer> iter = myIList.iterator();
		while (iter.hasNext()) {
			int anotherThing = iter.next();
		} //while

		endTime = System.nanoTime();
		runTime = endTime - startTime;
		System.out.println("Get from IteratingLL " +runTime);


	}
}
