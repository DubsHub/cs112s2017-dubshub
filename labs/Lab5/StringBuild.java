/**
 * Christian Walker Lab 5 Part 1
 */
public class StringBuild
{
  public static void main(String args[])
  {
    long startTime = System.nanoTime();

    StringBuilder mySB = new StringBuilder();

    for(int i = 0; i < 100000; i++)
    {
      mySB.append("a");
    }
    long endTime = System.nanoTime();

    long runTime = endTime - startTime;

    System.out.println("mySB" +mySB);
    System.out.println(runTime);

  }
}
