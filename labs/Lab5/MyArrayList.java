public class MyArrayList {

	private int[] data;
	private int nextLocation;

	public MyArrayList() {
		nextLocation = 0;
		data = new int[2];
	} //MyArrayList (constructor)

	public void add(int newData) {
		if (nextLocation >= 0.5*data.length) {   // if array is more than half-full
			resize(2*data.length);
		} //if

		data[nextLocation] = newData;
		nextLocation++;
		System.out.println("Inserted " + newData);
	} //add

	public int get(int index) {
		if (index < nextLocation) {
			return data[index];
		} else {
			throw new ArrayIndexOutOfBoundsException("index parameter too large");
		} //if-else
	} //get

	public void remove(int index) {
		if (index < nextLocation) {

			data[index] = -1;

			//for (int i = nextLocation-1; i >= index+1; i--) {
			for (int i = index+1; i <= nextLocation-1; i++) {
				data[i - 1] = data[i];
			} //for

			nextLocation--;

			if (nextLocation <= (0.25 * data.length)) {
				resize((int)(0.5 * data.length));
			} //if

		} else {
			throw new ArrayIndexOutOfBoundsException("index parameter too large");
		} //if
	} //remove

	public void resize(int newSize) {
		int oldSize = data.length;
		int newArray[] = new int[newSize];

		for (int i = 0; i < nextLocation; i++) {
			newArray[i] = data[i];
		} //for

		data = newArray;

		System.out.println("Resizing from " + oldSize + " to " + newSize + "\n");

	} //resize

	public int size() {
		return nextLocation;
	} //size

	public int arraySize() {
		return data.length;
	} //arraySize

} //MyArrayList (class)
