import java.io.BufferedReader; /* reads text */
import java.io.IOException; /* when fails */
import java.io.InputStreamReader; /* reads and returns single char */

/**
 * Christian Walker Lab 4 Part 2 Main
 */
public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter The Plain Text"); //
        String msg = br.readLine();

        System.out.println("The Encrypted Text");
        System.out.println(CaesarCipher.encrypt(msg, 3)); // calls for encrypt

        System.out.println("The Decrypted Text");
        System.out.println(CaesarCipher.decrypt(CaesarCipher.encrypt(msg, 3), 3)); // calls for decrypt
    }
}
