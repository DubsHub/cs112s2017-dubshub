/**
 * Christian Walker Lab 4 Part 2
 */
public class CaesarCipher {
    public static final String alphabet = "abcdefghijklmnopqrstuvwxyz";//string alphabet 
    //encrypt code 
    public static String encrypt(String plaint, int key) {
        plaint = plaint.toLowerCase(); //encrypt uppercase
        String ciphert = "";
        for (int i = 0; i < plaint.length(); i++) {
            int charpos = alphabet.indexOf(plaint.charAt(i)); //pos of alphabet
            int keyval = (charpos + key) % 26; //swap 26 letters in alphabet 
            char replaceval = alphabet.charAt(keyval);
            ciphert = ciphert + replaceval;
        }
        return ciphert;
    } //end encrypt
        //start decrypt
    public static String decrypt(String ciphert, int key) {
        ciphert = ciphert.toLowerCase(); //decrypt lowercase
        String plaint = "";
        for (int i = 0; i < ciphert.length(); i++) {
            int charpos = alphabet.indexOf(ciphert.charAt(i));
            int keyval = (charpos - key) % 26;
            if (keyval < 0) {
                keyval = alphabet.length() + keyval;
            }
            char replaceval = alphabet.charAt(keyval);
            plaint = plaint + replaceval;
        }
        return plaint;
    } //end decrypt
}
