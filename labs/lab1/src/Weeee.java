public class Weeee {
//accessable by any class
    public static void weeee() {
    		System.out.println("Weeee!");
                //output: "weeee!"
		weeee();
    } //weeee

    public static void main(String[] args) {
		weeee();
    } //indicates gvm where start point of program and how its executed

} //end Weeee class
