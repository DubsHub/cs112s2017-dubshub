public class Queue<T> implements ExceptionQueue<T>{

    int size, front, back;
    T[] array;

    public Queue(){
        array = (T[]) new Object[32];
        front = 0;
        back = 0;
        size = 0;
    }

    public void add(T data){
        if (size == array.length) {
            resize(2*array.length);
        }

        array[back] = data;
        back++;
        size++;
    }

    public T peek(){
        if (size == 0){
            throw new New("The Queue is empty");
        }
        return array[front];
    }

    public T remove() throws New{
        if (size == 0){
            throw new New("The Queue is empty");
        }

        T top = array[front];
        array[front] = null;
        front++;
        size--;
        return top;
    }

    public int size(){
        return size;
    }

    public boolean empty(){
        if (size == 0){
            return true;
        } else {
            return false;
        }
    }

    public void resize(int newSize){
        int oldSize = array.length;
        T newArray[] = (T[]) new Object[newSize];

            for (int i = 0; i < back; i++){
                newArray[i] = array[i];
            }
        array = newArray;
    }
}

